package com.peter.layouttest;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by petere on 5/15/17.
 */

public class KukuProducts extends AppCompatActivity {
    ListView kukuproducts;
    List <String>kukuprod;
    protected  void onCreate(Bundle b){
        super.onCreate(b);

        setContentView(R.layout.kukuproducts);

        kukuproducts=(ListView)findViewById(R.id.lstkukuproducts);

        createKukuProducts();

        ArrayAdapter kukuadapter=new ArrayAdapter(this,android.R.layout.simple_list_item_1,kukuprod);
        kukuproducts.setAdapter(kukuadapter);

        AdapterView.OnItemClickListener kukufeedAdapter=new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if(i==0){
                    //startActivity(new Intent(KukuFeeding.this,KukuFeeds.class));
                }
            }
        };
        //kukuproducts.setOnClickListener(kukufeedAdapter);
    }

    public void createKukuProducts(){
        kukuprod=new ArrayList<String>();
        kukuprod.add("Chease");
        kukuprod.add("Kuku egs");
        kukuprod.add("Toast breads");
        kukuprod.add("Kuku meat");
        kukuprod.add("Kuku gizards");
        kukuprod.add("Kuku batter");
        kukuprod.add("Mash mala");
        kukuprod.add("Kuku feathers");
        kukuprod.add("Kuku battered bread");
    }
}


