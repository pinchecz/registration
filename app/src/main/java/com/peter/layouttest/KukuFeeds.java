package com.peter.layouttest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by petere on 5/8/17.
 */

public class KukuFeeds extends AppCompatActivity {
    private RecyclerView recyclerView;
    private List<Movies> movieslist=new ArrayList<>();
    private KukuAdapter kukuAdapter;

    protected void onCreate(Bundle saveInstanceState){
        super.onCreate(saveInstanceState);
        setContentView(R.layout.kukufeeds);

        kukuAdapter=new KukuAdapter(movieslist);
        recyclerView=(RecyclerView) findViewById(R.id.kukufeedList);
        RecyclerView.LayoutManager kukuLayoutFeedMan=new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(kukuLayoutFeedMan);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(kukuAdapter);

        generateKukuFeedList();

    }
    protected void generateKukuFeedList(){
        movieslist.add(new Movies("Rambo defenderbles",1998,"Its the best action movi"));
        movieslist.add(new Movies("The last Stand",2017,"Its the best action movi"));
        movieslist.add(new Movies("Rise of an empire",2015,"Its the best action movi"));
        movieslist.add(new Movies("Death in the Jungle",2003,"Its the best action movi"));
        movieslist.add(new Movies("Fight to death",2008,"Its the best action movi"));
        movieslist.add(new Movies("Epensive Love",2002,"Its the best action movi"));
        movieslist.add(new Movies("Born to die for a moment",2011,"Its the best action movi"));
        movieslist.add(new Movies("The 300 spartans",2015,"Its the best action movi"));
        movieslist.add(new Movies("Love in the wild",2658,"Its the best action movi"));
        movieslist.add(new Movies("Come to birth",2010,"Its the best action movi"));
        movieslist.add(new Movies("Must generate children",2000,"Its the best action movi"));
        movieslist.add(new Movies("Born again in Yahshua",1999,"Its the best action movi"));
        movieslist.add(new Movies("Sterlon steve and the mane",1992,"Its the best action movi"));
        movieslist.add(new Movies("Beaten twice again",2013,"Its the best action movi"));


        movieslist.add(new Movies("Rambo defenderbles",1998,"Its the best action movi"));
        movieslist.add(new Movies("The last Stand",2017,"Its the best action movi"));
        movieslist.add(new Movies("Rise of an empire",2015,"Its the best action movi"));
        movieslist.add(new Movies("Death in the Jungle",2003,"Its the best action movi"));
        movieslist.add(new Movies("Fight to death",2008,"Its the best action movi"));
        movieslist.add(new Movies("Epensive Love",2002,"Its the best action movi"));
        movieslist.add(new Movies("Born to die for a moment",2011,"Its the best action movi"));
        movieslist.add(new Movies("The 300 spartans",2015,"Its the best action movi"));
        movieslist.add(new Movies("Love in the wild",2658,"Its the best action movi"));
        movieslist.add(new Movies("Come to birth",2010,"Its the best action movi"));
        movieslist.add(new Movies("Must generate children",2000,"Its the best action movi"));
        movieslist.add(new Movies("Born again in Yahshua",1999,"Its the best action movi"));
        movieslist.add(new Movies("Sterlon steve and the mane",1992,"Its the best action movi"));
        movieslist.add(new Movies("Beaten twice again",2013,"Its the best action movi"));

        movieslist.add(new Movies("Rambo defenderbles",1998,"Its the best action movi"));
        movieslist.add(new Movies("The last Stand",2017,"Its the best action movi"));
        movieslist.add(new Movies("Rise of an empire",2015,"Its the best action movi"));
        movieslist.add(new Movies("Death in the Jungle",2003,"Its the best action movi"));
        movieslist.add(new Movies("Fight to death",2008,"Its the best action movi"));
        movieslist.add(new Movies("Epensive Love",2002,"Its the best action movi"));
        movieslist.add(new Movies("Born to die for a moment",2011,"Its the best action movi"));
        movieslist.add(new Movies("The 300 spartans",2015,"Its the best action movi"));
        movieslist.add(new Movies("Love in the wild",2658,"Its the best action movi"));
        movieslist.add(new Movies("Come to birth",2010,"Its the best action movi"));
        movieslist.add(new Movies("Must generate children",2000,"Its the best action movi"));
        movieslist.add(new Movies("Born again in Yahshua",1999,"Its the best action movi"));
        movieslist.add(new Movies("Sterlon steve and the mane",1992,"Its the best action movi"));
        movieslist.add(new Movies("Beaten twice again",2013,"Its the best action movi"));
    }
}
