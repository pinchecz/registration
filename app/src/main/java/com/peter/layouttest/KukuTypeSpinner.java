package com.peter.layouttest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by petere on 5/15/17.
 */

public class KukuTypeSpinner extends AppCompatActivity  {

   protected void onCreate(Bundle b){
       super.onCreate(b);
       setContentView(R.layout.kukutypespinner);

       Spinner spinner=(Spinner) findViewById(R.id.spkukutypes);

       List<String> kukutypesSpinner=new ArrayList<String>();
       kukutypesSpinner.add("Broillers");
       kukutypesSpinner.add("Layers");
       kukutypesSpinner.add("Chick");
       kukutypesSpinner.add("Kienyeji");


       ArrayAdapter<String> kukutypesAdapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,kukutypesSpinner);

       spinner.setAdapter(kukutypesAdapter);
   }
}
