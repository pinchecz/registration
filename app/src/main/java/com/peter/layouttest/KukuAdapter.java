package com.peter.layouttest;


import android.app.LauncherActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by petere on 5/8/17.
 */

public class KukuAdapter extends RecyclerView.Adapter<KukuAdapter.KukuViewHolder>{

private List<Movies> movies;

    public class KukuViewHolder extends RecyclerView.ViewHolder{
        TextView title,genre,year;
        public KukuViewHolder(View itemView) {
            super(itemView);
            year=(TextView) itemView.findViewById(R.id.year);
            genre=(TextView) itemView.findViewById(R.id.genre);
            title=(TextView) itemView.findViewById(R.id.titles);
        }
    }

    public KukuAdapter(List<Movies> movies){
        this.movies=movies;
    }
    @Override
    public KukuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.kukufeedlist,parent,false);
        return new KukuViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(KukuViewHolder holder, int position) {
    Movies movilist=movies.get(position);
        holder.genre.setText(movilist.getMoviewGenre());
        holder.title.setText(movilist.getMovieName());
    }


    @Override
    public int getItemCount() {
        return movies.size();
    }
}
