package com.peter.layouttest;

/**
 * Created by petere on 5/8/17.
 */

public class Movies {
    private String movieName;
    private int movieYear;
    private  String moviewGenre;

    public  Movies(String name,int year,String genre){
        this.movieName=name;
        this.moviewGenre=genre;
        this.movieYear=year;
    }

    public void setMovieName(String name){
    this.movieName=name;
    }

    public void setMovieYear(int yr){
        this.movieYear=yr;
    }

    public void setMoviewGenre(String genre){
        this.moviewGenre=genre;
    }

    public String getMovieName(){
        return this.movieName;
    }

    public String getMoviewGenre(){
        return this.moviewGenre;
    }
    public int getMovieYear(){
        return this.movieYear;
    }
}
