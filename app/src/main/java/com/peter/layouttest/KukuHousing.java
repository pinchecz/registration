package com.peter.layouttest;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by petere on 4/28/17.
 */

public class KukuHousing extends AppCompatActivity {
    Button back;
    protected  void onCreate(Bundle bundle){
        super.onCreate(bundle);
        setContentView(R.layout.kukuhousing);

        back=(Button)findViewById(R.id.btnBack);


        back.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(KukuHousing.this,KukuDashboard.class);
                startActivity(intent);
            }
        });


    }
}
