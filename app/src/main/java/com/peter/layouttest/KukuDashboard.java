package com.peter.layouttest;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by petere on 4/28/17.
 */

public class KukuDashboard extends AppCompatActivity {
    Button kukufeeding,kukudeseases,kukuhousing,recyclerview,kukuproducts,kukutypes,kukuregistration;
    protected  void onCreate(Bundle bundle){
        super.onCreate(bundle);
        setContentView(R.layout.kukudashboard);

        kukudeseases=(Button)findViewById(R.id.btnkukudeseases);
        kukufeeding=(Button)findViewById(R.id.btnkukufeeding);
        kukuhousing=(Button)findViewById(R.id.btnkukuhousing);
        kukuproducts=(Button)findViewById(R.id.kukuproducts);
        recyclerview = (Button)findViewById(R.id.recyclerview);
        kukutypes = (Button)findViewById(R.id.btnkukutypes);
        kukuregistration = (Button)findViewById(R.id.kukureg);

        kukudeseases.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(KukuDashboard.this,KukuDeseases.class));
            }
        });

        kukufeeding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(KukuDashboard.this,KukuFeeding.class));
            }
        });

        kukuhousing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(KukuDashboard.this,KukuHousing.class));
            }
        });

        kukuproducts.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                startActivity(new Intent(KukuDashboard.this,KukuProducts.class));
            }
        });

        recyclerview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(KukuDashboard.this,KukuFeeds.class));
            }
        });

        kukutypes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(KukuDashboard.this,KukuTypeSpinner.class));
            }
        });

        kukuregistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(KukuDashboard.this,KukuRegistration.class));
            }
        });
    }
}
