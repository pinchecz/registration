package com.peter.layouttest;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;

/**
 * Created by petere on 4/28/17.
 */

public class KukuFeeding extends AppCompatActivity{
    Button back;
    ListView kukufeeds;
    protected  void onCreate(Bundle bundle){
        super.onCreate(bundle);
        setContentView(R.layout.kukufeeding);

        back=(Button)findViewById(R.id.btnBack);

        AdapterView.OnItemClickListener kukufeedAdapter=new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if(i==0){
                    startActivity(new Intent(KukuFeeding.this,KukuFeeds.class));
                }
            }
        };

        kukufeeds=( ListView)findViewById(R.id.kukufeeds);
       // kukufeeds.setOnClickListener(kukufeedAdapter);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(KukuFeeding.this,KukuFeeds.class));
            }
        });


    }
}
